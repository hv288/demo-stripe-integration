if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const stripeSecKey = process.env.STRIPE_SECRET_KEY;
const stripePubKey = process.env.STRIPE_PUBLIC_KEY;
const express = require("express");
const app = express();
const fs = require("fs");
const stripe = require("stripe")(stripeSecKey);

app.set("view engine", "ejs");
app.use(express.json());
app.use(express.static("public"));
app.get("/hello", (req, res) => {
  return res.send("hello H");
});

app.get("/store", (req, res) => {
  console.log("get store");
  fs.readFile("items.json", (err, data) => {
    if (err) {
      return res.status(500).end();
    }
    return res.render("store.ejs", {
      stripePubKey: stripePubKey,
      items: JSON.parse(data),
    });
  });
});

app.post("/purchase", (req, res) => {
  console.log("Purchase");
  fs.readFile("items.json", (err, data) => {
    if (err) {
      return res.status(500).end();
    }
    const itemsJson = JSON.parse(data);
    const itemsArray = itemsJson.music.concat(itemsJson.merch);
    let total = 0;
    console.log("===", itemsJson);
    console.log("===", itemsArray);
    console.log("===items", req.body.items);
    req.body.items.forEach((item) => {
      const itemJson = itemsArray.find((i) => i.id == item.id);
      console.log(itemJson);
      total = total + itemJson.price * item.quantity;
    });
    console.log("OK fine", total);
    stripe.charges
      .create({
        amount: total,
        source: req.body.stripeTokenId,
        currency: "usd",
      })
      .then(() => {
        console.log("Successful charged");
        res.json({ message: "Successfully purchased item" });
      })
      .catch(() => {
        console.log("Charge fail");
        res.status(500).end;
      });
  });
});

app.listen(3000);
